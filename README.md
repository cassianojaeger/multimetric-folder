# getmetric-folder

This script enhance current multimetric script to run based on a root folder.

## Usage
```shell 
usage ./getmetric folder [log_file_path]

Default log file destination is where the script is run.
```