#!/bin/sh
files=()
for entry in $1/*
do
	files+=$entry
done

defaultLogLocation=$(pwd)/log.js

if [ -z $2 ]
then
	multimetric ${files[@]} > $defaultLogLocation
else
	multimetric ${files[@]} > $2
fi
